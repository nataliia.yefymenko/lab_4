﻿namespace Lab_4
{
    internal class Program
    {
        static void Main()
        {
            string directoryPath;
            bool isValidDirectory = false;

            do
            {
                Console.Write("Enter the directory path: ");
                directoryPath = Console.ReadLine();

                if (Directory.Exists(directoryPath))
                {
                    isValidDirectory = true;
                }
                else
                {
                    Console.WriteLine("Invalid directory path. Please try again.");
                }
            } while (!isValidDirectory);

            CalculateSubdirectorySizes(directoryPath);

            Console.WriteLine("Press any key to exit.");
            Console.ReadKey();
        }

        static void CalculateSubdirectorySizes(string directoryPath)
        {
            try
            {
                string[] subdirectories = Directory.GetDirectories(directoryPath);

                foreach (string subdirectory in subdirectories)
                {
                    long directorySize = GetDirectorySize(subdirectory);
                    Console.WriteLine($"Directory size {subdirectory}: {FormatSize(directorySize)}");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error: {ex.Message}");
            }
        }

        static long GetDirectorySize(string directoryPath)
        {
            long totalSize = 0;

            try
            {
                string[] files = Directory.GetFiles(directoryPath);
                string[] subdirectories = Directory.GetDirectories(directoryPath);

                foreach (string file in files)
                {
                    FileInfo fileInfo = new FileInfo(file);
                    totalSize += fileInfo.Length;
                }

                foreach (string subdirectory in subdirectories)
                {
                    totalSize += GetDirectorySize(subdirectory);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error: {ex.Message}");
            }

            return totalSize;
        }

        static string FormatSize(long bytes)
        {
            const int scale = 1024;
            string[] sizeSuffixes = { "B", "KB", "MB", "GB", "TB" };

            int suffixIndex = 0;
            double scaledSize = (double)bytes;

            while (scaledSize >= scale && suffixIndex < sizeSuffixes.Length - 1)
            {
                scaledSize /= scale;
                suffixIndex++;
            }

            return $"{scaledSize:N1} {sizeSuffixes[suffixIndex]}";
        }
    }
}